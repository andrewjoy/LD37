﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public sealed class MemoryProjectile : MonoBehaviour
{
    private const float LAUNCH_ANGLE = 60.0f;
    private const float GRAVITY = 9.81f;

    private const float TIME_SCALE = 0.5f;

    [SerializeField]
    private MemoryPool poolPrefab = null;

    private Vector3 velocity = Vector3.zero;

    public CharacterController CharacterController { get; private set; }

    public void LaunchAt(Vector3 target)
    {
        this.velocity = target - this.transform.position;

        this.velocity.y = 0.0f;

        Quaternion launchRotation = Quaternion.LookRotation(this.velocity.normalized)
            * Quaternion.AngleAxis(-MemoryProjectile.LAUNCH_ANGLE, Vector3.right);

        this.velocity = launchRotation
            * Vector3.forward
            * Mathf.Sqrt((Mathf.Max(3.0f, this.velocity.magnitude) * MemoryProjectile.GRAVITY) / Mathf.Sin(MemoryProjectile.LAUNCH_ANGLE * Mathf.Deg2Rad * 2.0f));
    }

    private void Awake()
    {
        this.CharacterController = this.gameObject.GetComponent<CharacterController>();

        GameObject.Destroy(this.gameObject, 100.0f);
    }

    private void Update()
    {
        this.velocity += new Vector3(0.0f, -MemoryProjectile.GRAVITY * Time.deltaTime * MemoryProjectile.TIME_SCALE, 0.0f);

        this.CharacterController.Move(this.velocity * Time.deltaTime * MemoryProjectile.TIME_SCALE);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        MemoryPool pool = (MemoryPool)Component.Instantiate(this.poolPrefab, this.transform.position, Quaternion.identity);

        GameObject.Destroy(this.gameObject);
    }
}
