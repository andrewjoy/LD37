﻿using System.Collections.Generic;

using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

[RequireComponent(typeof(CharacterController))]
public sealed class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    private readonly HashSet<Mirage> limitedVisibilityMirages = new HashSet<Mirage>();

    [SerializeField]
    private AudioSource effectSource = null;

    [SerializeField]
    private AudioClip stunAudio = null;

    [SerializeField]
    private Effect stunEffect = null;

    [SerializeField]
    private Transform[] velocityDisplacementChildren = null;

    [SerializeField]
    private float movementSpeed = 3.0f;

    private float stunTimeRemaining = 0.0f;

    public bool BlockInput { get; set; }

    public bool IsStunned
    {
        get
        {
            return this.stunTimeRemaining > 0.0f;
        }
    }

    public CharacterController CharacterController { get; private set; }

    public void Stun()
    {
        if (!this.IsStunned)
        {
            this.stunTimeRemaining = 3.0f;

            this.effectSource.PlayOneShot(this.stunAudio);
        }
    }

    private void Awake()
    {
        this.CharacterController = this.gameObject.GetComponent<CharacterController>();

        Player.Instance = this;
    }

    private void OnEnable()
    {
        Mirage.AddVisibilityProvider(this.transform);
    }

    private void OnDisable()
    {
        Mirage.RemoveVisibilityProvider(this.transform);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("Intro");

            return;
        }

        Camera mainCamera = Camera.main;

        if (this.stunTimeRemaining > 0.0f)
        {
            this.stunTimeRemaining -= Time.deltaTime;
        }

        if (this.stunTimeRemaining < 1.0f)
        {
            Vector3 displacement = mainCamera.transform.forward;

            displacement.y = 0.0f;

            Vector3 inputDirection = new Vector3(
                Input.GetAxis("Horizontal"),
                0.0f,
                Input.GetAxis("Vertical"));

            if (!this.BlockInput && inputDirection != Vector3.zero)
            {
                float inputDirectionMagnitude = inputDirection.magnitude;
                if (inputDirectionMagnitude > 1.0f)
                {
                    inputDirection /= inputDirectionMagnitude;
                }

                displacement = Quaternion.LookRotation(displacement) * (this.movementSpeed * inputDirection);

                this.CharacterController.Move(displacement * Time.deltaTime);

                this.transform.rotation = Quaternion.RotateTowards(
                    this.transform.rotation,
                    Quaternion.LookRotation(displacement),
                    (displacement.magnitude / this.movementSpeed) * 360.0f * Time.deltaTime);
            }

            float velocityDisplacementWeight = Weight.Eager(Weight.FromTime(WrapType.Cycle, Time.time, 0.2f));
            Vector3 velocity =
                Quaternion.Euler(0.0f, 20.0f * velocityDisplacementWeight, 0.0f)
                * this.CharacterController.velocity;

            for (int i = 0; i < this.velocityDisplacementChildren.Length; ++i)
            {
                float height = this.velocityDisplacementChildren[i].position.y - this.velocityDisplacementChildren[i].parent.position.y;

                Vector3 parentOffset = -velocity * (1.0f - Mathf.Clamp01(height)) * 0.25f * Mathf.Abs(velocityDisplacementWeight);

                parentOffset.y = height;

                this.velocityDisplacementChildren[i].position = Vector3.Lerp(
                    this.velocityDisplacementChildren[i].position,
                    this.velocityDisplacementChildren[i].parent.position + parentOffset,
                    5.0f * Time.deltaTime);
            }

            this.stunEffect.PlayBackward();
        }
        else
        {
            this.stunEffect.PlayForward();
        }

        HashSet<Mirage> limitedVisibilityCleanupMirages = new HashSet<Mirage>(this.limitedVisibilityMirages);

        RaycastHit[] hits = Physics.RaycastAll(this.transform.position + Vector3.up, -mainCamera.transform.forward, 30);
        for (int i = 0; i < hits.Length; ++i)
        {
            Mirage hitMirage = hits[i].transform.gameObject.GetComponent<Mirage>();
            if (hitMirage != null)
            {
                limitedVisibilityCleanupMirages.Remove(hitMirage);

                if (!hitMirage.LimitVisibility && this.limitedVisibilityMirages.Add(hitMirage))
                {
                    hitMirage.LimitVisibility = true;
                }
            }
        }

        foreach (Mirage limitedVisibilityCleanupMirage in limitedVisibilityCleanupMirages)
        {
            this.limitedVisibilityMirages.Remove(limitedVisibilityCleanupMirage);

            limitedVisibilityCleanupMirage.LimitVisibility = false;
        }

        Mirage.ProcessGlobalVisbility();
    }
}
