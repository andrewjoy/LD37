﻿using UnityEngine;

public sealed class MirageBody : MonoBehaviour
{
    [SerializeField]
    private Vector3 fadeinDirection = new Vector3(0.0f, 5.0f, 0.0f);

    [SerializeField]
    private bool hide = false;

    [SerializeField]
    private Renderer rendererComponent = null;

    [SerializeField]
    private Renderer[] subrendererComponents = null;

    private float minimumTransparency = 0.0f;
    private float visibility = 1.0f;

    public float MinimumTransparency
    {
        get
        {
            return this.minimumTransparency;
        }

        set
        {
            if (Mathf.Abs(this.minimumTransparency - value) > 0.001f)
            {
                this.minimumTransparency = value;

                this.SetTransparency();
            }
        }
    }

    public float Visibility
    {
        get
        {
            return this.visibility;
        }

        set
        {
            if (Mathf.Abs(this.visibility - value) > 0.001f)
            {
                float previousVisibility = this.visibility;

                this.visibility = value;

                this.transform.localPosition += this.fadeinDirection * (this.visibility - previousVisibility);

                this.SetTransparency();
            }
        }
    }

    public float ExtraVisionRangeSqr { get; private set; }
    public float ExtraVisionRange { get; private set; }

    public bool Hide
    {
        get
        {
            return this.hide;
        }

        set
        {
            this.hide = value;
        }
    }

    private void Awake()
    {
        this.Visibility = 0.0f;

        this.ExtraVisionRangeSqr = Mathf.Pow(Random.Range(1.0f, 7.0f), 2.0f);
        this.ExtraVisionRange = Mathf.Sqrt(this.ExtraVisionRangeSqr);
    }

    private void SetTransparency()
    {
        float transparency = 1.0f - this.visibility;

        if (transparency < this.MinimumTransparency)
        {
            transparency = this.MinimumTransparency;
        }

        this.rendererComponent.material.SetFloat("_Transparency", transparency);

        this.rendererComponent.enabled = this.visibility > 0.01f;

        for (int i = 0; i < this.subrendererComponents.Length; ++i)
        {
            this.subrendererComponents[i].material.SetFloat("_Transparency", transparency);

            this.subrendererComponents[i].enabled = this.rendererComponent.enabled;
        }
    }
}
