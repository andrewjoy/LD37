﻿using UnityEngine;

public sealed class InitialMirageVisibility : MonoBehaviour
{
    [SerializeField]
    private Mirage mirage = null;

    [SerializeField]
    private float visibility = 1.0f;

    private void Awake()
    {
        this.Apply();
    }

    private void LateUpdate()
    {
        this.Apply();
    }

    private void Apply()
    {
        if (!this.mirage.Hide)
        {
            foreach (MirageBody body in this.mirage.Body)
            {
                body.Visibility = this.visibility;
            }

            Component.Destroy(this);
        }
    }
}
