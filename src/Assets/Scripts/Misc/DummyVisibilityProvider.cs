﻿using UnityEngine;

public sealed class DummyVisibilityProvider : MonoBehaviour
{
    private void OnEnable()
    {
        Mirage.AddVisibilityProvider(this.transform);
    }

    private void OnDisable()
    {
        Mirage.RemoveVisibilityProvider(this.transform);
    }

    private void Update()
    {
        Mirage.ProcessGlobalVisbility();
    }
}
