﻿using System;

using UnityEngine;

public sealed class StoryCinematic : MonoBehaviour
{
    public event Action OnComplete = null;

    [SerializeField]
    private Movement[] movement = null;

    public bool IsPlaying { get; private set; }

    public void Play()
    {
        this.IsPlaying = true;
    }

    private void Update()
    {
        if (this.IsPlaying)
        {
            bool isComplete = true;

            for (int i = 0; i < this.movement.Length; ++i)
            {
                this.movement[i].Mover.position = Vector3.MoveTowards(
                    this.movement[i].Mover.position,
                    this.movement[i].Target.position,
                    this.movement[i].TranslationSpeed * Time.deltaTime);

                isComplete = isComplete && this.movement[i].Mover.position == this.movement[i].Target.position;

                this.movement[i].Mover.rotation = Quaternion.RotateTowards(
                    this.movement[i].Mover.rotation,
                    this.movement[i].Target.rotation,
                    this.movement[i].RotationSpeed * Time.deltaTime);

                isComplete = isComplete && this.movement[i].Mover.rotation == this.movement[i].Target.rotation;
            }

            this.IsPlaying = !isComplete;

            if (!this.IsPlaying && this.OnComplete != null)
            {
                this.OnComplete.Invoke();
            }
        }
    }

    [Serializable]
    public sealed class Movement
    {
        [SerializeField]
        private Transform mover = null;

        [SerializeField]
        private Transform target = null;

        [SerializeField]
        private float translationSpeed = 5.0f;

        [SerializeField]
        private float rotationSpeed = 360.0f;

        public Transform Mover
        {
            get
            {
                return this.mover;
            }
        }

        public Transform Target
        {
            get
            {
                return this.target;
            }
        }

        public float TranslationSpeed
        {
            get
            {
                return this.translationSpeed;
            }
        }

        public float RotationSpeed
        {
            get
            {
                return this.rotationSpeed;
            }
        }
    }
}
