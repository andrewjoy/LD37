﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Slerpy.Unity3D;

[RequireComponent(typeof(Mirage))]
public sealed class Memory : MonoBehaviour
{
    private const float TICK_INTERVAL = 0.75f;

    [SerializeField]
    private string displayName = null;

    [SerializeField]
    private Text displayText = null;

    [SerializeField]
    private Effect[] displayEffects = null;

    [SerializeField]
    private MemoryPulse pulsePrefab = null;

    private readonly List<MemoryPulse> pulses = new List<MemoryPulse>();

    [SerializeField]
    private MemoryProjectile projectilePrefab = null;

    private int projectilesRemaining = 6;

    public Mirage Mirage { get; private set; }

    private void Awake()
    {
        this.Mirage = this.gameObject.GetComponent<Mirage>();
    }

    private void Update()
    {
        if (!this.Mirage.Hide && Time.time % Memory.TICK_INTERVAL < Time.deltaTime)
        {
            if (this.displayText != null)
            {
                this.displayText.text = string.Concat("Memory of ", this.displayName);

                for (int i = 0; i < this.displayEffects.Length; ++i)
                {
                    this.displayEffects[i].Rewind();
                    this.displayEffects[i].PlayForward();
                }

                this.displayText = null;
            }

            if (this.projectilesRemaining > 0)
            {
                --this.projectilesRemaining;

                MemoryProjectile projectile = (MemoryProjectile)Component.Instantiate(this.projectilePrefab, this.transform.position, Quaternion.identity);
                projectile.LaunchAt(Player.Instance.transform.position);

                Physics.IgnoreCollision(Player.Instance.CharacterController, projectile.CharacterController);
            }
            else
            {
                this.projectilesRemaining += 12;

                MemoryPulse pulse = (MemoryPulse)Component.Instantiate(this.pulsePrefab, this.transform.position + Vector3.up, Quaternion.identity);
                pulse.transform.localScale = new Vector3(0.0f, 1.0f, 0.0f);
                this.pulses.Add(pulse);
            }
        }

        Vector3 pulseDeltaSize = new Vector3(8.0f, 0.0f, 8.0f) * Time.deltaTime;
        Vector3 playerDisplacement = Player.Instance.transform.position - this.transform.position;

        playerDisplacement.y = 0.0f;

        float playerDisplacementMagnitude = playerDisplacement.magnitude;

        for (int i = 0; i < this.pulses.Count; ++i)
        {
            this.pulses[i].transform.localScale += pulseDeltaSize;
            this.pulses[i].Renderer.material.SetFloat("_Transparency", Mathf.Pow(this.pulses[i].transform.localScale.x / 50.0f, 5.0f));

            if (this.pulses[i].transform.localScale.x >= 50.0f)
            {
                GameObject.Destroy(this.pulses[i].gameObject);

                this.pulses.RemoveAt(i);

                --i;
            }
            else if (Mathf.Abs((playerDisplacementMagnitude * 2.0f) - this.pulses[i].transform.localScale.x) < 1.0f)
            {
                Player.Instance.CharacterController.Move(playerDisplacement.normalized * 6.0f * Time.deltaTime);
            }
        }
    }
}
