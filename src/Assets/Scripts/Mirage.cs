﻿using System.Collections.Generic;

using UnityEngine;

public sealed class Mirage : MonoBehaviour
{
    private static readonly List<Mirage> all = new List<Mirage>();

    private static readonly List<VisibilityProvider> visibilityProviders = new List<VisibilityProvider>();
    private static readonly List<VisibilityPulse> visibilityPulses = new List<VisibilityPulse>();

    public static void AddVisibilityProvider(Transform target)
    {
        Mirage.visibilityProviders.Add(new VisibilityProvider(target));
    }

    public static float GetVisibilityProviderRange(Transform target)
    {
        for (int i = 0; i < Mirage.visibilityProviders.Count; ++i)
        {
            if (Mirage.visibilityProviders[i].target == target)
            {
                return Mathf.Sqrt(Mirage.visibilityProviders[i].rangeSqr);
            }
        }

        return 0.0f;
    }

    public static float SetVisibilityProviderRange(Transform target, float range)
    {
        for (int i = 0; i < Mirage.visibilityProviders.Count; ++i)
        {
            if (Mirage.visibilityProviders[i].target == target)
            {
                VisibilityProvider provider = Mirage.visibilityProviders[i];

                provider.rangeSqr = range * range;

                Mirage.visibilityProviders[i] = provider;
            }
        }

        return 0.0f;
    }

    public static void RemoveVisibilityProvider(Transform target)
    {
        for (int i = 0; i < Mirage.visibilityProviders.Count; ++i)
        {
            if (Mirage.visibilityProviders[i].target == target)
            {
                Mirage.visibilityProviders.RemoveAt(i);

                return;
            }
        }
    }

    public static void AddVisibilityPulse(Vector3 origin)
    {
        Mirage.visibilityPulses.Add(new VisibilityPulse(origin));
    }

    public static void ProcessGlobalVisbility()
    {
        for (int i = 0; i < Mirage.visibilityPulses.Count; ++i)
        {
            VisibilityPulse pulse = Mirage.visibilityPulses[i];

            pulse.spread += 10.0f * Time.deltaTime;

            if (pulse.spread >= 50.0f)
            {
                Mirage.visibilityPulses.RemoveAt(i);

                --i;
            }
            else
            {
                pulse.thickness = 2.0f * (1.0f - (pulse.spread / 50.0f));

                Mirage.visibilityPulses[i] = pulse;
            }
        }

        for (int i = 0; i < Mirage.all.Count; ++i)
        {
            Mirage.all[i].ProcessSelfVisibility();
        }
    }

    private static bool CheckVisibility(VisibilityProvider from, Transform to, float extraVisionRangeSqr)
    {
        Vector3 displacement = to.position - from.target.position;

        displacement.y = 0.0f;

        return displacement.sqrMagnitude <= from.rangeSqr + extraVisionRangeSqr;
    }

    [SerializeField]
    private bool hide = true;

    [SerializeField]
    private bool affectedByPulse = false;

    [SerializeField]
    private bool limitVisibility = false;

    [SerializeField]
    private Collider[] colliders = null;

    [SerializeField]
    private MirageBody[] body = null;

    private float minimumTransparency = 0.0f;

    public bool Hide
    {
        get
        {
            return this.hide;
        }

        set
        {
            this.hide = value;

            for (int i = 0; i < this.colliders.Length; ++i)
            {
                this.colliders[i].enabled = !this.hide;
            }
        }
    }

    public IEnumerable<Collider> Colliders
    {
        get
        {
            return this.colliders;
        }
    }

    public IEnumerable<MirageBody> Body
    {
        get
        {
            return this.body;
        }
    }

    public bool LimitVisibility
    {
        get
        {
            return this.limitVisibility;
        }

        set
        {
            this.limitVisibility = value;
        }
    }

    public void ProcessSelfVisibility()
    {
        this.minimumTransparency = Mathf.Lerp(
            this.minimumTransparency,
            this.LimitVisibility ? 0.5f : 0.0f,
            5.0f * Time.deltaTime);

        for (int i = 0; i < this.body.Length; ++i)
        {
            float visibilitySpeed = 5.0f * Time.deltaTime;

            bool isVisible = false;

            if (!this.Hide && !this.body[i].Hide)
            {
                for (int k = 0; !isVisible && k < Mirage.visibilityProviders.Count; ++k)
                {
                    isVisible = Mirage.CheckVisibility(Mirage.visibilityProviders[k], this.body[i].transform, this.body[i].ExtraVisionRangeSqr);
                }

                if (this.affectedByPulse)
                {
                    for (int k = 0; !isVisible && k < Mirage.visibilityPulses.Count; ++k)
                    {
                        Vector3 pulseDisplacement = this.body[i].transform.position - Mirage.visibilityPulses[k].origin;

                        pulseDisplacement.y = 0.0f;

                        isVisible = Mathf.Abs((pulseDisplacement.magnitude + this.body[i].ExtraVisionRange) - Mirage.visibilityPulses[k].spread) <= Mirage.visibilityPulses[k].thickness;

                        if (isVisible)
                        {
                            visibilitySpeed *= 2.0f;
                        }
                    }
                }
            }

            this.body[i].MinimumTransparency = this.minimumTransparency;

            this.body[i].Visibility = Mathf.Lerp(
                this.body[i].Visibility,
                isVisible ? 1.0f : 0.0f,
                visibilitySpeed);
        }
    }

    private void Awake()
    {
        this.Hide = this.Hide;
    }

    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            this.Hide = this.Hide;
        }
    }

    private void OnEnable()
    {
        Mirage.all.Add(this);
    }

    private void OnDisable()
    {
        Mirage.all.Remove(this);
    }

    private struct VisibilityProvider
    {
        public readonly Transform target;

        public float rangeSqr;

        public VisibilityProvider(Transform target)
        {
            this.target = target;

            this.rangeSqr = 5.0f * 5.0f;
        }
    }

    private struct VisibilityPulse
    {
        public readonly Vector3 origin;

        public float spread;
        public float thickness;

        public VisibilityPulse(Vector3 origin)
        {
            this.origin = origin;

            this.spread = 10.0f;
            this.thickness = 2.0f;
        }
    }
}
