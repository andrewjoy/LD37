﻿using System;
using System.Collections.Generic;

using UnityEngine;

public sealed class StoryStage : MonoBehaviour
{
    public event Action OnTrigger = null;

    [SerializeField]
    private AudioClip progressAudio = null;

    [SerializeField]
    private string instruction = null;

    [SerializeField]
    private float deltaVisionRange = 0.0f;

    [SerializeField]
    private StoryCinematic outroCinematic = null;

    [SerializeField]
    private Transform[] visibilityProviders = null;

    [SerializeField]
    private Mirage[] dependantMirages = null;

    [SerializeField]
    private MirageBody[] dependantMirageBodies = null;

    public AudioClip ProgressAudio
    {
        get
        {
            return this.progressAudio;
        }
    }

    public string Instruction
    {
        get
        {
            return this.instruction;
        }
    }

    public float DeltaVisionRange
    {
        get
        {
            return this.deltaVisionRange;
        }
    }

    public StoryCinematic OutroCinematic
    {
        get
        {
            return this.outroCinematic;
        }
    }

    public IEnumerable<Transform> VisibilityProviders
    {
        get
        {
            return this.visibilityProviders;
        }
    }

    public IEnumerable<Mirage> DependantMirages
    {
        get
        {
            return this.dependantMirages;
        }
    }

    public IEnumerable<MirageBody> DependantMirageBodies
    {
        get
        {
            return this.dependantMirageBodies;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        Player colliderPlayer = collider.gameObject.GetComponent<Player>();
        if (colliderPlayer != null)
        {
            if (this.OnTrigger != null)
            {
                this.OnTrigger.Invoke();
            }
        }
    }
}
