﻿using UnityEngine;

using Slerpy.Unity3D;

public sealed class Story : MonoBehaviour
{
    private int currentStage = -1;

    [SerializeField]
    private AudioSource effectSource = null;

    [SerializeField]
    private AnimatedText instructionText = null;

    [SerializeField]
    private Effect endScreenEffect = null;

    [SerializeField]
    private StoryStage[] stages = null;

    public StoryStage CurrentStage
    {
        get
        {
            if (this.CurrentCinematic == null && this.currentStage >= 0 && this.currentStage < this.stages.Length)
            {
                return this.stages[this.currentStage];
            }

            return null;
        }
    }

    public StoryCinematic CurrentCinematic { get; private set; }

    [ContextMenu("Progress")]
    public void Progress()
    {
        bool hadCinematic = false;

        if (this.CurrentCinematic != null)
        {
            this.CurrentCinematic.OnComplete -= this.Progress;

            this.CurrentCinematic = null;

            Player.Instance.BlockInput = false;

            hadCinematic = true;
        }

        if (this.CurrentStage != null)
        {
            if (!hadCinematic)
            {
                this.CurrentStage.OnTrigger -= this.Progress;

                if (this.CurrentStage.OutroCinematic != null)
                {
                    this.CurrentCinematic = this.CurrentStage.OutroCinematic;

                    this.CurrentCinematic.OnComplete += this.Progress;

                    this.CurrentCinematic.Play();

                    Player.Instance.BlockInput = true;

                    return;
                }
            }

            hadCinematic = false;

            foreach (Transform visibilityProvider in this.CurrentStage.VisibilityProviders)
            {
                Mirage.RemoveVisibilityProvider(visibilityProvider);
            }

            foreach (Mirage dependantMirages in this.CurrentStage.DependantMirages)
            {
                dependantMirages.Hide = true;
            }

            foreach (MirageBody dependantMirageBodies in this.CurrentStage.DependantMirageBodies)
            {
                dependantMirageBodies.Hide = true;
            }

            Mirage.AddVisibilityPulse(this.CurrentStage.transform.position);
        }

        ++this.currentStage;

        if (this.CurrentStage != null)
        {
            if (this.CurrentStage.ProgressAudio != null)
            {
                this.effectSource.PlayOneShot(this.CurrentStage.ProgressAudio);
            }

            Mirage.SetVisibilityProviderRange(
                Player.Instance.transform,
                Mirage.GetVisibilityProviderRange(Player.Instance.transform) + this.CurrentStage.DeltaVisionRange);

            foreach (Transform visibilityProvider in this.CurrentStage.VisibilityProviders)
            {
                Mirage.AddVisibilityProvider(visibilityProvider);
            }

            foreach (Mirage dependantMirages in this.CurrentStage.DependantMirages)
            {
                dependantMirages.Hide = false;
            }

            foreach (MirageBody dependantMirageBodies in this.CurrentStage.DependantMirageBodies)
            {
                dependantMirageBodies.Hide = false;
            }

            this.instructionText.rectTransform.anchoredPosition =
                ((Vector2)Camera.main.WorldToViewportPoint(Player.Instance.transform.position)
                    - new Vector2(0.5f, 0.5f)).normalized
                * 100.0f;
            this.instructionText.text = this.CurrentStage.Instruction;
            this.instructionText.Rewind();

            this.CurrentStage.OnTrigger += this.Progress;
        }
        else
        {
            Player.Instance.BlockInput = true;

            this.endScreenEffect.PlayForward();
        }
    }

    private void Start()
    {
        this.Progress();
    }

    private void OnDestroy()
    {
        this.CurrentCinematic = null;

        if (this.CurrentStage != null)
        {
            foreach (Transform visibilityProvider in this.CurrentStage.VisibilityProviders)
            {
                Mirage.RemoveVisibilityProvider(visibilityProvider);
            }
        }
    }
}
