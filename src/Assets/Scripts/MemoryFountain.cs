﻿using UnityEngine;

[RequireComponent(typeof(Mirage))]
public sealed class MemoryFountain : MonoBehaviour
{
    private const float TICK_INTERVAL = 7.0f;

    [SerializeField]
    private MemoryProjectile projectilePrefab = null;

    public Mirage Mirage { get; private set; }

    private void Awake()
    {
        this.Mirage = this.gameObject.GetComponent<Mirage>();
    }

    private void Update()
    {
        if (!this.Mirage.Hide && Time.time % MemoryFountain.TICK_INTERVAL < Time.deltaTime)
        {
            for (int i = 0; i < 6; ++i)
            {
                Vector3 targetPosition = Random.insideUnitCircle * 8.0f;

                targetPosition.z = targetPosition.y;
                targetPosition.y = 0.0f;

                targetPosition += this.transform.position;

                MemoryProjectile projectile = (MemoryProjectile)Component.Instantiate(this.projectilePrefab, this.transform.position, Quaternion.identity);
                projectile.LaunchAt(targetPosition);

                Physics.IgnoreCollision(Player.Instance.CharacterController, projectile.CharacterController);
            }
        }
    }
}
