﻿using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public sealed class MemoryPool : MonoBehaviour
{
    [SerializeField]
    private Mirage mirage = null;

    [SerializeField]
    private Transform[] jiggleChildren = null;

    [SerializeField]
    private Effect despawnEffect = null;

    private bool hasFallen = false;

    private float jiggleWeight = 0.0f;

    public void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }

    private void Start()
    {
        if (!this.mirage.Hide)
        {
            foreach (MirageBody body in this.mirage.Body)
            {
                body.Visibility = 1.0f;
            }
        }
    }

    private void Update()
    {
        RaycastHit hitInfo;
        if (!this.hasFallen && Physics.Raycast(this.transform.position, -Vector3.up, out hitInfo, 5.0f))
        {
            float fallSpeed = Mathf.Min(hitInfo.distance, 2.0f * Time.deltaTime);

            if (fallSpeed != 0.0f)
            {
                this.transform.position -= new Vector3(0.0f, Mathf.Min(hitInfo.distance, 2.0f * Time.deltaTime), 0.0f);
            }
            else
            {
                this.hasFallen = true;
            }
        }

        float previousJiggleWeight = this.jiggleWeight;
        this.jiggleWeight = Weight.FromTime(WrapType.Cycle, Time.time, 0.1f);
        float deltaJiggleWeight = this.jiggleWeight - previousJiggleWeight;

        Vector3 deltaJiggle = new Vector3(0.0f, 0.1f, 0.0f) * deltaJiggleWeight;

        for (int i = 0; i < this.jiggleChildren.Length; ++i)
        {
            this.jiggleChildren[i].localPosition += deltaJiggle;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        Player colliderPlayer = collider.gameObject.GetComponent<Player>();
        if (colliderPlayer != null)
        {
            colliderPlayer.Stun();

            despawnEffect.Speed *= 3.0f;
        }
    }
}
