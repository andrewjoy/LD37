﻿using UnityEngine;

public sealed class MemoryPulse : MonoBehaviour
{
    [SerializeField]
    private Renderer rendererComponent = null;

    public Renderer Renderer
    {
        get
        {
            return this.rendererComponent;
        }
    }
}
