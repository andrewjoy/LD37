﻿Shader "Custom/Stippled Transparent" {
	Properties {
        [HDR]
		_Color ("Color", Color) = (1,1,1,1)
        _Transparency ("Transparency", Float) = 0.0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 150
		
		CGPROGRAM
		#pragma surface surf Standard noforwardadd

		struct Input {
            float3 normal;
            float4 screenPos;
		};

        fixed _Transparency;

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			o.Albedo = _Color.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
            o.Alpha = _Color.a * (1.0f - _Transparency);

            float4x4 thresholdMatrix =
            {
                1.0f / 17.0f, 9.0f / 17.0f, 3.0f / 17.0f, 11.0f / 17.0f,
                13.0f / 17.0f, 5.0f / 17.0f, 15.0f / 17.0f, 7.0f / 17.0f,
                4.0f / 17.0f, 12.0f / 17.0f, 2.0f / 17.0f, 10.0f / 17.0f,
                16.0f / 17.0f, 8.0f / 17.0f, 14.0f / 17.0f, 6.0f / 17.0f
            };

            float4x4 rowMatrix = 
            {
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
            };

            float2 pos = IN.screenPos.xy / IN.screenPos.w;
            pos *= _ScreenParams.xy;
            clip(o.Alpha - thresholdMatrix[fmod(pos.x, 4)] * rowMatrix[fmod(pos.y, 4)]);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
